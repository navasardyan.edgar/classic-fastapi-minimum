# Project purpose #

The project is the implementation of the docs (https://fastapi.tiangolo.com/tutorial/sql-databases/)

This is a plainly educational project which purpose is to learn and demonstrate the minimal setup of database connection in FastApi project. It does not utilize SQLModel
# Key words: #

- create_engine
- makesession - Later we will inherit from this class to create each of the database models or classes (the ORM models)

# Interesting facts #
Note that you don't have to run alembic files at the beginning of the project because all files
are created using this piece of code

```python
models.Base.metadata.create_all(bind=engine)
```

